package controllers

import (
	"fmt"

	"github.com/astaxie/beego"
	fb "github.com/huandu/facebook"

	"golang.org/x/oauth2"
)

type MainController struct {
	beego.Controller
}

type Image struct {
	Height int
	Width  int
	Source string
}

type FacebookPhoto struct {
	Id          string
	Name        string
	CreatedTime string
	Images      []Image
}

//not used
type FacebookFriend struct {
	Id   string
	Name string
}

type FacebookTaggableFriendPictureData struct {
	IsSilhouette bool
	Url          string
}

type FacebookTaggableFriendPicture struct {
	Data FacebookTaggableFriendPictureData
}

type FacebookTaggableFriend struct {
	Id      string
	Name    string
	Picture FacebookTaggableFriendPicture
}

var fbPhoto FacebookPhoto

var fbFriend FacebookFriend //not used

var fbTaggableFriend FacebookTaggableFriend

var (
	oauthConf = &oauth2.Config{
		ClientID:     "535197553311956",
		ClientSecret: "3b2c925acd56c5af9af2a526f3e85984",
		RedirectURL:  "http://localhost:8080/oauthcode",
		Scopes:       []string{"email", "user_photos", "user_friends"},
		Endpoint: oauth2.Endpoint{
			AuthURL:  "https://www.facebook.com/dialog/oauth",
			TokenURL: "https://graph.facebook.com/oauth/access_token",
		},
	}
)

// starts here
func (c *MainController) Get() {
	fmt.Println("it's here")
	c.TplNames = "index.tpl"
}

// creates an Authorization Url to get the temporary code
func (c *MainController) Login() {
	url := oauthConf.AuthCodeURL("vish", oauth2.AccessTypeOnline) //state="vish" (arbitary string) to prevent CRSF attacks
	c.Redirect(url, 302)                                          //temporary redirect
}

//facebook redirects here
func (c *MainController) Oauthcode() {
	code := c.Ctx.Request.FormValue("code")                  //gets us the temporary code from facebook
	token, err := oauthConf.Exchange(oauth2.NoContext, code) //access token
	if err != nil {
		fmt.Printf("oauthConf.Exchange() failed with '%s'\n", err)
		c.Redirect("/", 302)
		return
	}
	oauthClient := oauthConf.Client(oauth2.NoContext, token)
	session := &fb.Session{
		Version:    "v2.4", //graph API version
		HttpClient: oauthClient,
	}
	//TaggableFriendList(session)
	PhotoList(session)
	c.TplNames = "oauthpage.tpl"
}

//friends that can be tagged
func TaggableFriendList(session *fb.Session) {
	count := 1
	res, _ := session.Get("/me/taggable_friends", nil) //json result from facebook
	paging, _ := res.Paging(session)                   //paging - to get all data
	results := paging.Data()                           //data from a particular page
	var endFlag bool
	endFlag = false //to check if it's the last page
	for endFlag == false {
		for _, result := range results {
			result.Decode(&fbTaggableFriend) //json to struct uses json.unmarshaler
			fmt.Print(count)
			fmt.Print(".")
			fmt.Println(fbTaggableFriend.Name)
			count++
		}
		noMore, _ := paging.Next() //next page
		results = paging.Data()
		endFlag = noMore
	}
}

//user photos
func PhotoList(session *fb.Session) {
	count := 1
	res, _ := session.Get("/me/photos", nil)
	paging, _ := res.Paging(session)
	results := paging.Data()
	var endFlag bool
	endFlag = false
	for endFlag == false {
		for _, result := range results {
			result.Decode(&fbPhoto)
			fmt.Print(count)
			fmt.Print(".")
			fmt.Println(fbPhoto.Id)
			count++
		}
		noMore, _ := paging.Next()
		results = paging.Data()
		endFlag = noMore
	}
}

// just checking
func (c *MainController) Thisway() {
	c.Redirect("/", 302)
}
