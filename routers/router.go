package routers

import (
	"facebookBeego/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/oauthcode", &controllers.MainController{}, "get:Oauthcode")
	beego.Router("/login", &controllers.MainController{}, "get:Login")
	beego.Router("/thisway", &controllers.MainController{}, "get:Thisway")
}
